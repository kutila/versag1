echo off
REM ***************************************************************************
REM Windows batch file to start JADE test container
REM 
REM
REM jade.log 	- prints log messages from JADE (uses Java Logging API)
REM versag.log	- prints log messages from VERSAG (uses Log4j)
REM 
REM Last updated 2009-06-23 by Kutila
REM ***************************************************************************

set VERSAG_HOME=.

REM add JADE libraries to classpath
set CLASSPATH=%VERSAG_HOME%\lib\JadeLeap.jar

REM add other libraries and folders to classpath
set CLASSPATH=%CLASSPATH%;%VERSAG_HOME%\lib\logformatter.jar;%VERSAG_HOME%\lib\concierge-1.0.0.RC3.jar;%VERSAG_HOME%\lib\versag.jar;%VERSAG_HOME%\conf

REM add "-Xms128m -Xmx512m" immediately after 'java' to increase memory allocation
java -Djava.util.logging.config.file=conf\jadelog.properties -Dmma.home=. jade.Boot -gui -container-name loc1 BOB:mma2.jade.JadeAgent(agent1.properties);JO:mma2.jade.JadeAgent(agent2.properties)

REM java -agentpath:"D:\dev\NetBeans 6.7.1\profiler3\lib\deployed\jdk15\windows\profilerinterface.dll=\"D:\dev\NetBeans 6.7.1\profiler3\lib\"",5140 -Djava.util.logging.config.file=conf\jadelog.properties -Dmma.home=. jade.Boot -gui -container-name loc1 BOB:mma2.jade.JadeAgent(agent1.properties)