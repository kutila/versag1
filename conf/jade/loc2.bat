echo off
REM ***************************************************************************
REM Windows batch file to start JADE test container
REM 
REM
REM jade.log 	- prints log messages from JADE (uses Java Logging API)
REM versag.log	- prints log messages from VERSAG (uses Log4j)
REM 
REM Last updated 2009-06-23 by Kutila
REM ***************************************************************************

set VERSAG_HOME=.

REM add JADE libraries to classpath
set CLASSPATH=%VERSAG_HOME%\lib\JadeLeap.jar

REM add other libraries and folders to classpath
set CLASSPATH=%CLASSPATH%;%VERSAG_HOME%\lib\logformatter.jar;%VERSAG_HOME%\lib\concierge-1.0.0.RC3.jar;%VERSAG_HOME%\lib\versag.jar;%VERSAG_HOME%\conf

REM options to be added for remote debugging JDK1.6
REM  -agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=8000

java -Djava.util.logging.config.file=conf\jadelog.properties jade.Boot -container -container-name loc2
