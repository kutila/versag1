/*
 * VERSAG project
 * 
 */

package mma2;

import java.util.Properties;
import mma2.capability.Repository;
import mma2.itinerary.ItineraryService;

/**
 * Represents a VERSAG agent.
 * 
 * @author kutila
 */
public interface AgentIF {

    public String getAgentStatus();
    
    public Repository getRepository();

    public ItineraryService getItineraryService();
    
    public void requestMove(String destination);

    public Properties getProperties();
    
    public String isAt();
    
    public String getAgentName();

    /**
     * Register a service to provide in the agent platform's Yellow Pages Service.
     * An agent can now register for only one service at a time. If this agent 
     * was previously registered to provide another service, that entry will be
     * removed before adding this service.
     *
     * @param type
     * @param value
     */
    public void registerInYellowPages(String type, String value);

    /**
     * Deregister the current service if the agent is already registered for
     * any service with the agent platform's Yellow Pages Service.
     */
    public void deregisterFromYellowPages();

    /**
     * Search for entries in the agent platform's Yellow Pages service
     *
     * @param type
     * @return An array of matching "values"
     */
    public String[] searchInYellowPages(String type);

    /**
     * This method wakes up the agent's kernel process such that the kernel
     * can execute a new command.
     * It should be called by terminating capabilities if it was an active
     * process and blocked the kernel from executing subsequent commands.
     * (For present applies to OneShot behaviours only)
     *
     * @since 2009/10/29
     */
    public void wakeUpKernel();

    /**
     * This method is only used to set the Previous Command which is 
     * a monitored variable thru STATUS Requests
     * 
     * @return
     * @since 2011/02/12
     */
    void setPreviousCommand(String previousCommand);
}
