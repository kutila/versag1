/*
 * VERSAG project
 * DSSE Centre, Monash University, Australia
 */
package mma2.bundles.common;

import mma2.AgentIF;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;

/**
 * This abstract class can be used as a template for creating Capabilities
 * with OneShot execution semantics.
 *
 * @author kutila2
 * created on 30/10/2009
 */
public abstract class AbstractOneShotClass extends Thread implements ServiceIF {

    protected BundleContext context;

    public AbstractOneShotClass(BundleContext context) {
        this.context = context;
    }

    /**
     * Obtain from the OSGi platform, a service registered under the
     * given classname.
     *
     * @param className
     * @return A reference to an object of the requested service type
     */
    protected Object getOSGiService(String className) {
        ServiceReference ref = context.getServiceReference(className);
        return context.getService(ref);
    }

    /**
     * A OneShot type capability when finishing should:
     *  1. stop itself (i.e. the OSGi bundle)
     *  2. wake up the kernel behaviour (call agent.wakeUpKernel() )
     */
    private void stopOneShotCapability() {
        Object agent = getOSGiService(AgentIF.class.getName());
        if (context != null) {
            try {
                context.getBundle().stop();
            } catch (Exception e) {
                System.out.println("Exception when trying to stop/uninstall bundle. <" +
                        e.toString() + ">");
            }
            if (agent != null) {
                ((AgentIF) agent).wakeUpKernel();
            }
        }
    }

    /**
     * If the Capability is to be prematurely terminated, the "working" thread has
     * to be stopped. A flag for such stopping should be implemented in the work()
     * method and setting the flag done inside this method.
     * The default implementation does nothing and has no effect on the "working"
     * thread.
     */
    public void requestStop() {
    }

    /**
     * The functionality of the Capability should be implemented in here. This
     * method will be executed in a new "working" Thread. Once this method returns,
     * the thread would terminate and the OSGi bundle also would be stopped.
     */
    public abstract void work();

    @Override
    public final void run() {
        work();
        stopOneShotCapability();
    }

}
