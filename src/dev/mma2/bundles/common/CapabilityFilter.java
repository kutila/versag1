/*
 * VERSAG project
 */
package mma2.bundles.common;

/**
 * The interface to be implemented by classes filtering Capabilities to match
 * a given set of criteria.
 * 
 * @author kutila
 * created 17/12/2008
 */
public interface CapabilityFilter {

    /**
     * Tests a Capability with the given description matches the filter.
     * 
     * @param capabilityDescription Description of Capability being matched
     * @return An object indicating the level of match. This could be a 
     * Boolean object in case of a filter which does not allow partial matches
     */
    public Object match(Object capabilityDescription);
    
}
