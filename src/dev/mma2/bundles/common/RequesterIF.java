/*
 * VERSAG project
 */
package mma2.bundles.common;


/**
 * The interface for a Capability requesting service. The "find" methods are 
 * asynchronous and would spawn new Threads to do the searching. 
 * Implementations of this interface should have PASSIVE execution type since
 * there is no continuously running Thread.
 *
 * @author kutila
 * created on 3/11/2008
 */
public interface RequesterIF extends ServiceIF {

    /**
     * Asynchronous method to search for and acquire a Capability by given ID.
     * 
     * If found, exactly one Capability is loaded onto the Repository. If not, 
     * a repository entry with the key "STATE_capabilityId" would be added
     * giving the reason for failure.
     * 
     * @param capabilityId Unique identifier of required Capability
     */
    public void findById(String capabilityId);

    /**
     * Asynchronous method to search for and acquire Capabilities matching
     * the given <i>Capability Specs</i>. Method implementations can
     * use different mechanisms to search for, filter and acquire matching
     * Capabilities. The needed capabilities should be loaded to the agent's
     * repository at the end.
     * 
     * @param specs
     */
    public void findBySpec(String[] specs);

}
