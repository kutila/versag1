/*
 * VERSAG project
 */
package mma2.bundles.common;

/**
 * An interface to be used by all OSGi bundles
 * 
 * @author kutila
 */
public interface ServiceIF {

    public void requestStop();
    
}
