/*
 * Capability.java
 *
 * Created on 6 November 2007, 18:00
 *
 */
package mma2.capability;

import java.io.Serializable;

/**
 * Represents a <i>Capability</i>.
 *
 * @author kutila
 */
public class Capability implements Serializable {
  
    /** Contains the "details" about the capability */
    public CapabilityDetails details;

    private byte[] contents;
    private String sourceCode = null;
//    private Vector<MethodDetails> methodDetails;

    /** Creates a new instance of Capability */
    public Capability() {
        details = new CapabilityDetails();
    }
    
//    public Vector<MethodDetails> getMethodDetails() {
//        return methodDetails;
//    }
//
//    public void setMethodDetails(Vector<MethodDetails> methodDetails) {
//        this.methodDetails = methodDetails;
//    }

    public String getSourceCode() {
        return sourceCode;
    }

    public void setSourceCode(String sourceCode) {
        this.sourceCode = sourceCode;
    }

    public byte[] getContents() {
        return contents;
    }

    public void setContents(byte[] contents) {
        this.contents = contents;
    }
   
    @Override
    public String toString() {
        StringBuffer sbuf = new StringBuffer();
        sbuf.append(details.toString());
        sbuf.append(':');
        sbuf.append(contents.length);
/*        sbuf.append("<methodDetails:");
        for (Iterator<MethodDetails> it = methodDetails.iterator(); it.hasNext();) {
            sbuf.append(it.next().toString());
            sbuf.append(' ');
        }
        sbuf.append('>');
  */
        return sbuf.toString();
    }
    
}
