/*
 * VERSAG project
 * DSSE Centre, Monash University, Australia
 */
package mma2.capability;

import java.util.Vector;

/**
 * Class representing "details" of a Capability. Every capability
 * should have a CapabilityDetails object describing it.
 * 
 * @author kutila
 */
public class CapabilityDetails extends CapabilitySpec {

    /**
     * Capability grouping based on execution semantics
     * PASSIVE - no thread
     * ONESHOT - new thread which runs to completion
     * CYCLIC - new thread which runs continuously
     */
    public static enum EXECUTION_TYPE {PASSIVE, ONESHOT, CYCLIC}
    
    public static enum ACCESS_LEVEL {OWNER, GROUP, ALL}
    public static enum AGENT_PLATFORMS {JADE4, JADE3_7, JADE3_5, JADE2, GH2_2}

    private String id;
    private String owner;
    private String group;
    private String author;
    private String version;
    private EXECUTION_TYPE type;
    private ACCESS_LEVEL accessLevel;
    private boolean separateThreadRecommended = false;
    private boolean optimized = false;
    private String originURL;
    private String description;

    //an array of identifiers for agents who have this Capability
    private Vector holdingAgents = new Vector();

    private int size = 0;
    private int runningNwLoad = 0;
    private long estimatedRunningTime;
    private long estimatedStartingTime;
    private int heapSizeRating;
    private int cpuRating;
    private int accuracyRating;

    public long getEstimatedRunningTime() {
        return estimatedRunningTime;
    }

    public void setEstimatedRunningTime(long estimatedExecutionTime) {
        this.estimatedRunningTime = estimatedExecutionTime;
    }

    public long getEstimatedStartingTime() {
        return estimatedStartingTime;
    }

    public void setEstimatedStartingTime(long estimatedStartingTime) {
        this.estimatedStartingTime = estimatedStartingTime;
    }

    public int getAccuracyRating() {
        return accuracyRating;
    }

    public void setAccuracyRating(int accuracyRating) {
        this.accuracyRating = accuracyRating;
    }

    public int getCpuRating() {
        return cpuRating;
    }

    public void setCpuRating(int cpuRating) {
        this.cpuRating = cpuRating;
    }

    public int getHeapSizeRating() {
        return heapSizeRating;
    }

    public void setHeapSizeRating(int heapSizeRating) {
        this.heapSizeRating = heapSizeRating;
    }

    public int getRunningNwLoad() {
        return runningNwLoad;
    }

    public void setRunningNwLoad(int runningNwLoad) {
        this.runningNwLoad = runningNwLoad;
    }


    
    public void setId(String id) {
        this.id = id;
    }
    public String getId() {
        return id;
    }

    public EXECUTION_TYPE getExecutionType() {
        return type;
    }

    public void setExecutionType(EXECUTION_TYPE type) {
        this.type = type;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public ACCESS_LEVEL getAccessLevel() {
        return accessLevel;
    }

    public void setAccessLevel(ACCESS_LEVEL accessLevel) {
        this.accessLevel = accessLevel;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public boolean isOptimized() {
        return optimized;
    }

    public void setOptimized(boolean optimized) {
        this.optimized = optimized;
    }

    public boolean isSeparateThreadRecommended() {
        return separateThreadRecommended;
    }

    public void setSeparateThreadRecommended(boolean separateThreadRecommended) {
        this.separateThreadRecommended = separateThreadRecommended;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public String getOriginURL() {
        return originURL;
    }

    public void setOriginURL(String URL) {
        this.originURL = URL;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Object[] getHoldingAgents() {
        return holdingAgents.toArray();
    }

    public void addHoldingAgent(Object ag) {
        holdingAgents.add(ag);
    }

    /**
     * Returns true if this capability instance satisfies the requirements
     * set by the given Capability specification.
     *
     * @param spec the capability specification to match against
     * @return true in case of a match, false otherwise
     */
    public boolean match(CapabilitySpec spec) {
        // 1) check if required functions are supported
        Object[] implementedFunctions = this.getFunctions();
        Object[] fsToMatch = spec.getFunctions();
        for (Object f : fsToMatch) {
            boolean found = false;
            for (Object implementedF : implementedFunctions) {
                if (f.equals(implementedF)) {
                    found = true;
                    break;
                }
            }
            if (found == false) {
                return false;
            }
        }

        // 2) check if required platforms are supported
        AGENT_PLATFORMS[] supportedPlatforms = this.getAgentPlatforms();
        AGENT_PLATFORMS[] platforms = spec.getAgentPlatforms();
        for (AGENT_PLATFORMS p : platforms) {
            boolean found = false;
            for (AGENT_PLATFORMS supportedP : supportedPlatforms) {
                if (p.equals(supportedP)) {
                    found = true;
                    break;
                }
            }
            if (found == false) {
                return false;
            }
        }

        return true;
    }


    @Override
    public String toString() {
        StringBuffer sbuf = new StringBuffer();
        sbuf.append(id);
        sbuf.append(':');
        sbuf.append(version);
        sbuf.append(':');
        sbuf.append(owner);
        sbuf.append(':');
        sbuf.append(group);
        sbuf.append(':');
        sbuf.append(description);
        sbuf.append(':');
        sbuf.append(agentPlatforms);
        sbuf.append(": size=");
        sbuf.append(size);
        sbuf.append(": run_time=");
        sbuf.append(estimatedRunningTime);
        sbuf.append(": run_load=");
        sbuf.append(runningNwLoad);
        sbuf.append(": accuracy=");
        sbuf.append(accuracyRating);
        sbuf.append(": heap=");
        sbuf.append(heapSizeRating);
        sbuf.append(": cpu=");
        sbuf.append(cpuRating);
        return sbuf.toString();
    }
}
