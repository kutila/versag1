/*
 * VERSAG project
 * DSSE Centre, Monash University, Australia
 */
package mma2.capability;

import java.io.Serializable;
import mma2.capability.CapabilityDetails.*;

/**
 * This class is a specification describing the functionality provided
 * by a Capability.
 * Multiple capability "instances" would match a given capability specification.
 *
 * @author kutila
 * created on 10/11/2009
 */
public class CapabilitySpec implements Serializable {

    /** Supported agent platforms */
    protected AGENT_PLATFORMS[] agentPlatforms;

    /** Functionality implemented by the Capability */ //TODO - how to describe
    protected Object[] functions;

    /** NOTE:  [24/06/2010]
     * Constraints are not part of a CapabilitySpec and do not belong
     * here. However, this is the easiest way to get them from the itinerary String
     * to the capability/module where constraints are built for adaptation
     * decision making.
     */
    protected Object[] constraints;

    protected String alias;

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }
    
    public AGENT_PLATFORMS[] getAgentPlatforms() {
        return agentPlatforms;
    }

    public void setAgentPlatforms(AGENT_PLATFORMS[] agentPlatforms) {
        this.agentPlatforms = agentPlatforms;
    }

    public Object[] getFunctions() {
        return functions;
    }

    public void setFunctions(Object[] functions) {
        this.functions = functions;
    }

    public Object[] getConstraints() {
        return constraints;
    }

    public void setConstraints(Object[] constraints) {
        this.constraints = constraints;
    }


    @Override
    public String toString() {
        StringBuffer b = new StringBuffer("[");
        for (Object f : functions) {
            b.append(f.toString());
            b.append(" ");
        }
        b.append(" , ");
        for (AGENT_PLATFORMS p : agentPlatforms) {
            b.append(p);
            b.append(" ");
        }
        b.append("]");
        return b.toString();
    }

    @Override
    public boolean equals(Object o) {
        throw new UnsupportedOperationException(
                "This class cannot be compared for equality. " +
                "Use the CapabilityDetails.match() method instead."
                );
    }
}
