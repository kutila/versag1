/*
 * VERSAG project
 */
package mma2.capability;

import java.io.Serializable;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

/**
 * The Repository Service is where Capabilities and other Serializables are stored
 * and carried by the agent.
 * 
 * Steps to make the Repository available to OSGi bundles:
 * - export mma2.capability as a core package when starting OSGi
 * - Register the Repository as an OSGi service
 * - Bundles accessing the Repository should import mma2.capability package
 * 
 * @author kutila
 */
public class Repository implements Serializable {

    public static final String STATE_PREFIX = "STATE_";
    public static final String COMMAND_PREFIX = "CMD_";
    public static final String UNAVAILABLE = "unavailable";
    public static final String KEY_AUTO_RESTART = "auto.start";
    public static final String KEY_PROVIDER_LIST = "providers";
    
    
    /** The Repostory carried around by the agent */
    private Map<String, Serializable> map = Collections.synchronizedMap(new HashMap());

    /**
     * Convenience method to add a Capability to the Repository
     * 
     * @param path
     * @param value
     */
    public void setCapability(String path, Capability value) {
        map.put(path, value);
    }
    
    /**
     * Convenience method to retrieve a Capability
     * @param path
     * @return The Capability if available
     */
    public Capability getCapability(String path) {
        return (Capability) map.get(path);
    }

    /**
     * Convenience method to remove a Capability from the Repository
     * @param path
     * @return The removed Capability
     */
    public Capability removeCapability(String path) {
        return (Capability) map.remove(path);
    }
    
    /**
     * Get all the Capabilities in the Repository as a Collection
     */
    public Collection<Capability> getCapabilities() {
        Collection values = map.values();
        Collection<Capability> collection = new Vector<Capability>();
        for (Object object : values) {
            if (object instanceof Capability) {
                collection.add((Capability) object);
            }
        }
        return collection;
    }
    
    /**
     * Store a Serializable object in the Repository
     * @param path
     * @param value
     */
    public void set(String path, Serializable value) {
        map.put(path, value);
    }
    
    /**
     * Retrieve a Serializable object from the Repository
     * @param path
     * @return The Serializable object, if available
     */
    public Serializable get(String path) {
        return map.get(path);
    }

    /**
     * Get all the keys in the Repository
     */
    public Set<String> keySet() {
        return map.keySet();
    }
    
    /**
     * Remove a Serializable object from the Repository
     * @param path
     * @return The removed object
     */
    public Serializable remove(String path) {
        return map.remove(path);
    }

    /**
     * Check whether an entry with the given path exists in the Repository.
     * @param path
     * @return true if an entry exists, false otherwise
     */
    public boolean containsEntry(String path) {
        return map.containsKey(path);
    }
     
    /**
     * Get the current size of the Repository
     * @return Size of the repository
     */
    public int getSize() {
        return map.size();
    }

    /**
     * Adds new entries to the Repository. 
     * Entries should be in name, value pairs, each pair separated from the next 
     * by a new line.
     * E.g. path1=value1
     *      path2=value2
     *      ...
     * Entries not matching this format are ignored. Existing entries with the
     * same path will be overwritten.
     * 
     * @param parameters
     */
    public void setParameters(String parameters) {
        String[] parameterLines = parameters.split("\n");//split according to lines
        //note: the newline character could cause problems on non-windows platforms
        for (int i = 0; i < parameterLines.length; i++) {
            String[] tmp = parameterLines[i].split("=");
            if (tmp != null && tmp.length == 2) {
                set(tmp[0], tmp[1]);
            }
        }
    }
    
    @Override
    public String toString() {
        return map.toString();
    }
}
