/*
 * VERSAG project
 */
package mma2.itinerary;

import java.io.Serializable;

/**
 * A Command represents an activity to be executed by the Agent's Kernel. 
 * Usually it has an associated "location" at which it should be executed. 
 * A String representation of the command is also available.
 * A Command also has a status.
 * 
 * A Command however does not implement the GoF Command pattern fully. 
 * (i.e. it does not have the execution logic)
 * 
 * @author kutila
 */
public class Command implements Serializable {
    
    public static enum STATUS {
        CREATED,
        EXECUTING,
        EXECUTED,
        STARTED,
        FAILED_NOT_LOADED,
        FAILED_ERROR,
        FAILED_NO_REQUESTER_SERVICE,
        FAILED_INVALID_CMD,
        FAILED_NOT_FOUND
    }

    public static enum TYPE {
        MOVE,
        TERMINATE,
//        LOAD,
        UNLOAD,
        FIND,
        START,
        STOP,
        NULL
    }

    //----- the supported itinerary actions -----
    public static final String MOVE = "move";
    public static final String FIND = "find";
    public static final String START = "start";
    public static final String STOP = "stop";
//    public static final String LOAD = "load";
    public static final String UNLOAD = "unload";
    public static final String TERMINATE = "terminate";
    public static final String SEP_CHAR = "#";
    
    
    /**
     * The location where this command should be executed. A NULL value
     * indicates that it can be executed at any location.
     */
    private String location = null;
    
    /** The status of this command */
    private STATUS status;

    /** The type of this command */
    private TYPE type;

    /** For a command being executed, there maybe an associated bundle */
    private String associatedBundleName = null;

    /** For a MOVE command, the destination name */
    private String destination = null;
    
    /**
     * A String representation of the command. If the command is based on the
     * itinerary it could be the relevant itinerary String
     */
    private String commandString = "";

    /**
     * Other parameters associated with the Command
     * For example, a FIND command may have multiple parameters
     */
    private String[] parameters;

    /** Constructor */
    public Command(String location) {
        this.location = location;
        status = STATUS.CREATED;
    }
    
    public String getCommandString() {
        return commandString;
    }

    public void setCommandString(String itineraryCmd) {
        this.commandString = itineraryCmd;
    }

    public STATUS getStatus() {
        return status;
    }

    public void setStatus(STATUS status) {
        this.status = status;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public TYPE getType() {
        return type;
    }

    public void setType(TYPE type) {
        this.type = type;
    }

    public String getAssociatedBundleName() {
        return associatedBundleName;
    }

    public void setAssociatedBundleName(String associatedBundleName) {
        this.associatedBundleName = associatedBundleName;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String[] getParameters() {
        return parameters;
    }

    public void setParameters(String[] parameters) {
        this.parameters = parameters;
    }
    
    @Override
    public String toString() {
        StringBuffer buf = new StringBuffer();
        buf.append('<');
        buf.append(getCommandString());
        buf.append(':');
        buf.append(getLocation());
        buf.append(':');
        buf.append(getStatus());
        buf.append(':');
        buf.append(getType());
        buf.append(':');
        buf.append(getAssociatedBundleName());
        buf.append(':');
        buf.append(getDestination());
        buf.append('>');
        return buf.toString();
    }
    
}
