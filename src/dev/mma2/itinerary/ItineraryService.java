/*
 * VERSAG project
 */
package mma2.itinerary;

import jade.util.Logger;
import java.io.Serializable;
import java.util.Hashtable;

import mma2.itinerary.Command.STATUS;
import mma2.itinerary.Command.TYPE;

/**
 * Itinerary service implementation.
 * The itinerary service is provided by the Agent.
 * 
 * 
 * @author kutila
 */
public class ItineraryService implements Serializable {

    public static final String SEP_CHAR = "#";
    
    /** Agent's itinerary represented as a String */
    private String itinerary;
    
    /** Contains {location}:{itinerary} pairs */
    private Hashtable<String, Command[]> itineraryTable = new Hashtable();
    
    /** Current location */
    private String here;
    
    /** When the itinerary was last updated */
    private long lastUpdated = -1;

    public synchronized void setHere(String here) {
        //use the "local name" only if full GUID based name is given
        //NEEDS TO BE IMPROVED/FIXED
        this.here = here;//.split("@", 2)[0];
    }

    private synchronized String getHere() {
        return here;
    }

    public String getItinerary() {
        return itinerary;
    }

    /**
     * Set a new itinerary discarding any previous ones.
     * 
     * @param itinerary String representation of the itinerary
     */
    public synchronized void setItinerary(String itinerary) {
        log("setting Itinerary <" + itinerary + ">");
        lastUpdated = System.currentTimeMillis(); //TODO: need a globally correct timestamp
        this.itinerary = itinerary;
        itineraryTable.clear();

        //--populate the itineraryTable with the new itinerary
        String[] itineraryLines = itinerary.split("\n");//split according to lines
//        log("no. of lines in itinerary = " + itineraryLines.length);
        
        for (int i = 0; i < itineraryLines.length; i++) {
            //E.g.: locx=fetch...#exec...#move...
            String[] tmp = itineraryLines[i].split("="); //E.g. {locx, fetch...#exec...}
            String location = tmp[0];
            String[] actions = tmp[1].split(SEP_CHAR); //E.g. {fetch... , exec... , ...}
            Command[] commands = new Command[actions.length];
            
//            log("location    = " + location);
//            log("actions     = " + tmp[1]);
//            log("no. of cmds = " + actions.length);
            
            for (int j = 0; j < actions.length; j++) {
                commands[j] = generateCommand(location, actions[j]);
            }
            itineraryTable.put(location, commands);
        }
    }

    /**
     * Get the next itinerary Command to execute for the "current" location 
     * or a dummy Command if no command is available.
     */
    public synchronized Command getItineraryCommand() {
        Command[] commands = null;
        if (isItineraryAvailable()) {
            commands = itineraryTable.get(getHere());
            if (commands != null && commands.length > 0) {
                for (Command c : commands) {
                    switch (c.getStatus()) {
                        case CREATED:
                            return c;
                        default:
                            continue;
                    }
                }
            }
        }
        Command tmp = new Command(null);
        tmp.setStatus(STATUS.CREATED);
        tmp.setType(TYPE.NULL);
        return tmp;
    }

    /**
     * Get the itinerary records for the given location
     * 
     * @return an array of matching Command objects or null if an itinerary
     * is not available.
     */
    public synchronized Command[] getItineraryCommands(String location) {
        if (isItineraryAvailable()) {
            return itineraryTable.get(location);
        }
        return null;
    }

    /**
     * Allows the updating of a subset of the itinerary
     * 
     * added 2010-06-10
     * @param location
     * @param commands
     */
    public synchronized Command[] setItineraryCommands(String location, Command[] commands) {
        if (location == null || location.trim().length() == 0) {
            return null;
        }
        if (commands != null || commands.length > 0) {
            Command[] oldValues = itineraryTable.put(location, commands);
            return oldValues;
        }
        return null;
    }

    public synchronized boolean isItineraryAvailable() {
        return itineraryTable != null && !itineraryTable.isEmpty();
    }

    public long getLastUpdated() {
        return lastUpdated;
    }

    //--------- private methods ----------
    
    /**
     * Generates a Command with status CREATED and necessary parameters
     * set.
     * Incomplete action strings result in Commands being created 
     * with STATUS FAILED_INVALID_CMD.
     */
    private Command generateCommand(String location, String action) {
        Command cmd = new Command(location);
        cmd.setCommandString(action);

        String[] params = action.split("\\s");
        String cmdType = params[0];
        
        if (cmdType.equals(Command.MOVE)) {
            cmd.setType(TYPE.MOVE);
        } else if (cmdType.equals(Command.FIND)) {
            cmd.setType(TYPE.FIND);
        } else if (cmdType.equals(Command.START)) {
            cmd.setType(TYPE.START);
        } else if (cmdType.equals(Command.STOP)) {
            cmd.setType(TYPE.STOP);
//        } else if (action.startsWith(ItineraryCommand.LOAD)) {
//            cmd.setType(TYPE.LOAD);
        } else if (cmdType.equals(Command.UNLOAD)) {
            cmd.setType(TYPE.UNLOAD);
        } else if (cmdType.equals(Command.TERMINATE)) {
            cmd.setType(TYPE.TERMINATE);
        } else {
            cmd.setType(TYPE.NULL); //an unknown command
            log("Created NULL itinerary command for: " + action);
        }

        switch (cmd.getType()) {
            case MOVE:
                if (params.length < 2) {
                    cmd.setStatus(STATUS.FAILED_INVALID_CMD);
                } else {
                    cmd.setStatus(STATUS.CREATED);
                    cmd.setDestination(params[1]);
                }
                break;
            case FIND:
                if (params.length < 2) {
                    cmd.setStatus(STATUS.FAILED_INVALID_CMD);
                } else {
                    cmd.setStatus(STATUS.CREATED);
                    String[] newArray = new String[params.length-1];
                    System.arraycopy(params, 1, newArray, 0, newArray.length);
                    cmd.setParameters(newArray);
                }
                break;
            case START:
            case STOP:
            case UNLOAD:
                if (params.length < 2) {
                    cmd.setStatus(STATUS.FAILED_INVALID_CMD);
                } else {
                    cmd.setStatus(STATUS.CREATED);
                    cmd.setAssociatedBundleName(params[1]);
                }
                break;
            case TERMINATE:
            case NULL:
            default:
                cmd.setStatus(STATUS.CREATED);
                break;
        }
        
        log("Generated command " + cmd.toString());
        return cmd;
    }

    /**This is not a very good way of logging. Just doing it for debugging purposes.
     * Have to decide later on whether Logging should be available!
     */
    private void log(Object o) {
        Logger logger = Logger.getMyLogger(this.getClass().getName());
        logger.fine(o.toString());
    }
}
