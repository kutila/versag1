/*
 * VERSAG project
 */
package mma2.jade;

import mma2.capability.Repository;
import mma2.*;
import mma2.itinerary.ItineraryService;

import jade.content.lang.sl.SLCodec;
import jade.core.Agent;
import jade.core.Location;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.domain.FIPANames;
import jade.domain.mobility.MobilityOntology;
import jade.util.Logger;

import java.io.Serializable;
import java.util.Iterator;
import java.util.Properties;
import java.util.Set;
import java.util.Vector;
import mma2.capability.Capability;

/**
 *
 * @author kutila
 */
public class JadeAgent extends Agent implements AgentIF {

    transient Logger logger = Logger.getMyLogger(this.getClass().getName());

    /** Contains the agent properties read at startup time */
    private Properties properties = null;

    /** The capability repostory carried around by the agent */
    private Repository repository = null;

    /** The Itinerary Service maintained by the agent */
    private ItineraryService itineraryService = null;

    private KernelBehaviour kernelBehaviour;
    
    /** Only for monitoring purposes thru STATUS Requests [2011-02-12] */
    private String previousCommand;
    
    /**
     * Setup is called only once when the agent is being created. 
     */
    @Override
    public void setup() {
        jadeSetup();
        
        //--initialize the capabilityStore and load with the initial setCapability of capabilities
        //setCapabilityStore(new HashMap());
        repository = new Repository();
        repository.set("NAME", getLocalName());
        itineraryService = new mma2.itinerary.ItineraryService();
        itineraryService.setHere(here().getName());//agent's current location
        //logger.fine("Address is = " + here().getAddress());
        
        //--add Behaviours
        kernelBehaviour = new KernelBehaviour(this); //Kernel with OSGi support
        addBehaviour(kernelBehaviour);
        addBehaviour(new MessageListeningBehaviour(this)); //handle incoming messages
        
        logger.info(getLocalName() + " setup successfully");
    }

    @Override
    public void takeDown() {
        logger.info(getLocalName() + " is being taken down");
        deregisterFromYellowPages();
        kernelBehaviour.stop();
        //clean up any other held resources
    }

    //----- house keeping tasks for cloning
    @Override
    protected void beforeClone() {
        System.out.println(getLocalName() + " is now cloning itself");
    }

    @Override
    protected void afterClone() {
        System.out.println(getLocalName() + " has cloned itself.");
        afterMove();
    }

    /**
     * This method is executed just before moving the agent to another
     * location. It is automatically called by the JADE framework.
     */
    @Override
    protected void beforeMove() {
        logger.info(getLocalName() + " is moving elsewhere");
        deregisterFromYellowPages();
        kernelBehaviour.stop();
        //clean up other resources
    }

    /**
     * This method is executed as soon as the agent arrives at the new
     * destination.
     */
    @Override
    protected void afterMove() {
        logger = Logger.getMyLogger(this.getClass().getName());
        logger.info(getLocalName() + " arrived at " + here().getName());
        jadeSetup();
        itineraryService.setHere(here().getName());//current location
    }

    public void afterLoad() {
        afterClone();
    }

    public void beforeFreeze() {
        beforeMove();
    }

    public void afterThaw() {
        afterMove();
    }

    public void beforeReload() {
        beforeMove();
    }

    public void afterReload() {
        afterMove();
    }

    /**
     * This method is only used to monitor Agent status.
     * 
     * @return
     * Implemented [2011-02-12]
     */
    public String getAgentStatus() {
        char OPEN_BRACKET = '[';
        char CLOSE_BRACKET = ']';

        StringBuilder builder = new StringBuilder();
        
        //agent name
        builder.append(OPEN_BRACKET);
        builder.append(getAgentName()); 
        builder.append(CLOSE_BRACKET);
        
        //current location
        builder.append(OPEN_BRACKET);
        builder.append(here().getName()); 
        builder.append(CLOSE_BRACKET);
        
        //itinerary
        builder.append(OPEN_BRACKET);
        builder.append(itineraryService.getItinerary());
        builder.append(CLOSE_BRACKET);
        
        //current itinerary position - TODO
        builder.append(OPEN_BRACKET);
        builder.append(previousCommand);
        builder.append(CLOSE_BRACKET);
        
        //bundles carried
        builder.append(OPEN_BRACKET);
        Set<String> keySet = repository.keySet();
        for (String key : keySet) {
            Serializable val = repository.get(key);
            if (val instanceof Capability) {
                String id = ((Capability) val).details.getId();
                builder.append(id);
                builder.append(" ,");
            }
        }
        builder.append(CLOSE_BRACKET);

        //parameters
        builder.append(OPEN_BRACKET);
        keySet = repository.keySet();
        for (String key : keySet) {
            Serializable val = repository.get(key);
            if (val instanceof String) {
                builder.append(key);
                builder.append('=');
                builder.append(val);
                builder.append(" ,");
            }
        }
        builder.append(CLOSE_BRACKET);
        
        return builder.toString();
    }

    public void setPreviousCommand(String previousCommand) {
        this.previousCommand = previousCommand;
    }
    
    /**
     * Get the agent configuration properties read at agent start time.
     * 
     * @return Properties object
     */
    @Override
    public Properties getProperties() {
        return properties;
    }

    @Override
    public String getAgentName() {
        return super.getLocalName();
    }
    
    
//    public String getProperty(String key) {
//        if (properties != null) {
//            return properties.getProperty(key);
//        }
//        return null;
//    }
    
    @Override
    public String isAt() {
        return here().getName();
    }

    private String registeredService = null;

    public void registerInYellowPages(String type, String details) {
        deregisterFromYellowPages();

        ServiceDescription sd = new ServiceDescription();
        sd.setType(type);
        sd.setName(details);

        DFAgentDescription dfd = new DFAgentDescription();
        dfd.setName(this.getAID());
        dfd.addServices(sd);

        try {
            DFService.register(this, dfd);
            registeredService = type;
        } catch (Exception e) {
            e.printStackTrace();
            logger.warning("Error registering in YellowPages: " + type + "=" + details);
        }
    }

    public void deregisterFromYellowPages() {
        if (registeredService == null) {
            return;
        }
        DFAgentDescription dfd = new DFAgentDescription();
        dfd.setName(this.getAID());
        ServiceDescription sd = new ServiceDescription();
        sd.setType(registeredService);

        dfd.addServices(sd);
        try {
            DFService.deregister(this, dfd);
            registeredService = null;
        } catch (Exception e) {
            logger.warning("Error deregistering in YellowPages for " + registeredService);
        }

    }

    public String[] searchInYellowPages(String type) {
        DFAgentDescription template = new DFAgentDescription();
        ServiceDescription sd = new ServiceDescription();
        if (type != null) {
            sd.setType(type);
        }
        template.addServices(sd);

        Vector<String> v = new Vector<String>();
        try {
            DFAgentDescription[] services = DFService.search(this, template);
            for (DFAgentDescription dFAgentDescription : services) {
                Iterator it = dFAgentDescription.getAllServices();
                while (it.hasNext()) {
                    ServiceDescription object = (ServiceDescription) it.next();
                    v.add(object.getName());
                }
            }
        } catch (Exception e) {
            logger.warning("Error searching YellowPages for " + type);
        }
        return v.toArray(new String[0]);
    }

    private void jadeSetup() {
        String propertyFileName = null;
        // Read and setCapability agent Properties
        Object[] args = getArguments();
        if (args != null && args.length > 0) {
            propertyFileName = args[0].toString();
        }
        if (propertyFileName != null && propertyFileName.trim().length() > 0) {
            try {
                properties = new Properties();
                properties.load(new java.io.FileInputStream(
                        "conf" + java.io.File.separatorChar + propertyFileName));
            } catch (Exception e) {
                logger.warning("Failed to load Properties from " + propertyFileName + e.toString());
            }
        }
        // Register SL0 content language and JADE mobility ontology
        getContentManager().registerLanguage(new SLCodec(), FIPANames.ContentLanguage.FIPA_SL0);
        getContentManager().registerOntology(MobilityOntology.getInstance());
    }

    public Repository getRepository() {
        return repository;
    }

    public ItineraryService getItineraryService() {
        return itineraryService;
    }

    public void requestMove(String destination) {
        Location dest = new jade.core.ContainerID(destination, null);    
        doMove(dest);
    }

    public void wakeUpKernel() {
        logger.finest("kernel wakeup request received");
        if (kernelBehaviour != null) {
            kernelBehaviour.restart();
        }
    }

}
