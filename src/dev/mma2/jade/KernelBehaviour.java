/*
 * VERSAG project
 */
package mma2.jade;

import mma2.capability.Capability;
import mma2.AgentIF;
import mma2.capability.Repository;

import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;

import jade.util.Logger;
import java.util.Properties;
import java.util.Vector;
import mma2.bundles.common.RequesterIF;
import mma2.capability.CapabilityDetails.EXECUTION_TYPE;
import mma2.service.execution.ExecutorService;
import mma2.itinerary.Command;
import mma2.itinerary.Command.STATUS;
import mma2.itinerary.Command.TYPE;
import mma2.util.Utility;

/**
 * This class represents the Kernel module of the framework. Its tasks at present
 * are to:
 *  - start the OSGi engine (if not started)
 *  - execute Commands (obtained from the Itinerary Service) 
 *  - load core setCapability of bundles specified in agent parameters
 * 
 * TODO
 *  - handling command failures and adjusting subsequent behaviours accordingly
 * 
 * Temporary code:
 *  - sleep between successive runs
 * 
 * @author kutila
 */
public class KernelBehaviour extends CyclicBehaviour {

    /** a reference to the VERSAG Agent */
    private AgentIF agent;
    
    private Command previousCommand;
    private Vector<Command> cyclicCommands;
    
    private transient boolean initialized = false;
    private transient Logger logger = null;
    private transient ExecutorService executorService = null;

    public KernelBehaviour(Agent agent) {
        super(agent);
        this.agent = (AgentIF) agent;
        logger = Logger.getMyLogger(this.getClass().getName());

        populateRepositoryOnStartup();
        executorService = new ExecutorService();
        cyclicCommands = new Vector();
        logger.finest("Created");
    }

    @Override
    public void action() {
        reInitialize();
        
        Command command = agent.getItineraryService().getItineraryCommand();
        if (command == null || command.getType() == Command.TYPE.NULL || 
                !isPreviousCommandCompleted()) {
//            doArtificialDelay();
            //return; //Nothing to do
            block();
            logger.finest("<<Blocking>> " + command);
            //BUG 001: the behaviour should block here. In the else, executeCommand()
            //can be called. But the behaviour has to be restarted using restart()
            //when the time to execute the next itinerary command comes
        } else {
            executeCommand(command);
        }
    }

    /**
     * Stop the underlying services and relase any resources (e.g. before moving)
     */
    public void stop() {
        executorService.requestStop();
    }

    /* ---------- private methods ---------- */

    /** 
     * Load a core setCapability of bundles onto the Repository. List of 
     * bundles to load are specified through agent configuration file.
     */
    public void populateRepositoryOnStartup() {
        String errors = "";
        Properties p = agent.getProperties();
        Repository repository = agent.getRepository();
        
        int count = Integer.parseInt(p.getProperty(Utility.BUNDLE_COUNT, "0"));
        for (int i = 0; i < count; i++) {
            String bundleName = p.getProperty(Utility.BUNDLE_PREFIX + i + Utility.NAME);
            try {
                Capability cap = Utility.loadCapabilityFromFile(p, i);
                if (cap != null) {
                    repository.setCapability(bundleName, cap);
                    logger.finest(bundleName + " added to Repository");
                }
            } catch (Exception e) {
                errors += " " + e.getMessage();
            }
        }

        //load Parameters to be saved in the repository
        String parameters = p.getProperty(Utility.PARAMETERS, "");
        repository.setParameters(parameters);

        if (errors.length() > 0) {
            logger.warning("Following errors occured during Repository population:\n" + errors);
        }
    }

    /**
     * Re-initialize, possibly after agent migration
     *  - recreate transient objects (Logger, ExecutorService...)
     */
    private void reInitialize() {
        if (initialized) {
            //System.out.print("."); //DEBUG
            return; //already initialized, no need to do again
        }
        if (logger == null) {
            logger = Logger.getMyLogger(this.getClass().getName());
        }
        if (executorService == null) {
            executorService = new ExecutorService();
        }
        String corePkgs = agent.getProperties().getProperty(Utility.CORE_PACKAGES, "");
        executorService.init(corePkgs, agent.getAgentName());
        registerServices();
        restartCapabilitiesAfterMove();
        logger.finest("Initialized");
        initialized = true;
    }

    /**
     * Register agent services that need to be accessible from within bundles
     * Agent properties specify which services to register
     */
    private void registerServices() {
        Properties p = agent.getProperties();

        //--register special services
        if ("true".equals(p.getProperty(Utility.SERVICE_AGENT, "true"))) {
            String[] classes = {Agent.class.getName(), AgentIF.class.getName()};
            executorService.registerService(agent, classes);
        }
        if ("true".equals(p.getProperty(Utility.SERVICE_REPOSITORY, "true"))) {
            executorService.registerService(agent.getRepository(), Repository.class.getName());
        }

        //--register normal services (those which can be instantiated using Class.forName() )
        int count = Integer.parseInt(p.getProperty(Utility.SERVICE_COUNT, "0"));
        for (int i = 0; i < count; i++) {
            String className = p.getProperty(Utility.SERVICE_PREFIX + i + Utility.SERVICE_CLASS_SUFFIX);
            try {
                Class clss = Class.forName(className);
                Object obj = clss.newInstance();
                executorService.registerService(obj, className);
            } catch (Exception e) {
                logger.warning("Failed to register service: " + className + e.toString());
            }
        }
    }

    private void restartCapabilitiesAfterMove() {
        String restart = (String) agent.getRepository().get(Repository.KEY_AUTO_RESTART);
        if (restart == null || !restart.equals("FALSE")) {
            Vector<Command> v = cyclicCommands;
            cyclicCommands = new Vector();
            for (Command c : v) {
                logger.finest("Restarting command " + c.getCommandString());
                execute(c);
            }
        }
    }

    private boolean isPreviousCommandCompleted() {
        boolean returnVal = true; //default is true
        if (previousCommand != null) {
            switch (previousCommand.getStatus()) {
                case EXECUTED:
                case STARTED:
                    break;
                case FAILED_ERROR:
                case FAILED_INVALID_CMD:
                case FAILED_NOT_LOADED:
                case FAILED_NOT_FOUND:
                case FAILED_NO_REQUESTER_SERVICE:
                    //handleCommandFailure() -- at the moment we just ignore it
                    break;
                case EXECUTING:
                    switch (previousCommand.getType()) {
                        case TERMINATE:
                            previousCommand.setStatus(STATUS.FAILED_ERROR);
                            break;
                        case MOVE:
                            String destin = previousCommand.getCommandString().split("\\s")[1];
                            if (agent.isAt().equalsIgnoreCase(destin)) {
                                previousCommand.setStatus(STATUS.EXECUTED);
                            } else {
                                previousCommand.setStatus(STATUS.FAILED_ERROR);
                                returnVal = false;
                            }
                            break;
                        case START:
                            //looks like prevous command was a ONESHOT capability
                            STATUS stat = executorService.getBundleStatus(
                                    previousCommand.getAssociatedBundleName());
                            previousCommand.setStatus(stat);
                            if (STATUS.EXECUTING.equals(stat)) {
                                logger.finest(previousCommand + " is still EXECUTING");
                                returnVal = false;
                            }
                            break;
                        case FIND:
                            //Check repository for an indication of completion [2009-11-12]
                            Repository rep = agent.getRepository();
                            String findCmdStatus = (String) rep.get(Repository.COMMAND_PREFIX + "FIND");
                            if (findCmdStatus == null) {
                                logger.finest("previous FIND command seems to be still EXECUTING");
                                returnVal = false;
                            } else {
                                //not checking actual value, as its existence indicates completion
                                //Also, no longer checking if it Succeeded or failed
                                previousCommand.setStatus(STATUS.EXECUTED);
                                rep.remove(Repository.COMMAND_PREFIX + "FIND");
                                logger.finest("Updating previous FIND command as EXECUTED");
                                returnVal = true;
                            }
                            break;
                        default:
                            logger.warning("Unsupported previous command TYPE. " + previousCommand.toString());
                            returnVal = false;
                    }
                    break;
                default:
                    logger.warning("Unknown Previous command state. " + previousCommand.toString());
                    returnVal = false;
            }//end switch
        }
        logger.finest(previousCommand + " isPreviousCommandCompleted = " + returnVal);
        return returnVal;
    }
    
    private void executeCommand(Command command) {
        logger.info(agent.getAgentName() + " executing " + command.toString());
        TYPE type = command.getType();
        switch (type) {
            case MOVE:
                //update list of cyclic commands to be restarted at new location
                Vector<Command> temp = new Vector();
                for (Command c: cyclicCommands) {
                    STATUS stat = executorService.getBundleStatus(c.getAssociatedBundleName());
                    if (STATUS.EXECUTING.equals(stat)) {
                        temp.add(c);
                        logger.finest(c.getAssociatedBundleName() + " should be restarted after move");
                    }
                }
                cyclicCommands = temp;
                
                executorService.requestStop();
                agent.requestMove(command.getDestination());
                command.setStatus(STATUS.EXECUTING);
                initialized = false; //if move fails, causes restarting OSGi, services etc.
                break;
            case TERMINATE:
                executorService.requestStop();
                myAgent.doDelete(); //request agent to terminate itself
                command.setStatus(STATUS.EXECUTING);
                break;
            case STOP:
                executorService.stopAndUninstallBundle(command.getAssociatedBundleName());
                command.setStatus(STATUS.EXECUTED);
                break;
            case UNLOAD:
                executorService.stopAndUninstallBundle(command.getAssociatedBundleName());
                agent.getRepository().removeCapability(command.getAssociatedBundleName());
                command.setStatus(STATUS.EXECUTED);
                break;
            case START:
                STATUS stat = execute(command);
                command.setStatus(stat);
                break;
            case FIND:
                stat = find(command);
                command.setStatus(stat);
                break;
            default:
                logger.fine("Unknown command: " + type);
                command.setStatus(STATUS.EXECUTED);
        }
        previousCommand = command;
        agent.setPreviousCommand(previousCommand.toString());
    }
    
    /**
     * Get the capability from the repository and pass on to the
     * Executor service to install and run
     * 
     * @param cmd The command to be executed
     * @return The execution status
     */
    private STATUS execute(Command cmd) {
        //--validate Command
        String itineraryRecord = cmd.getCommandString();
        if ((itineraryRecord == null) || (itineraryRecord.trim().length() == 0)) {
            logger.warning("Unexecutable command " + cmd.toString());
            return STATUS.FAILED_INVALID_CMD;
        }
        String bundleName = cmd.getAssociatedBundleName();
        
        //--get Capability from Repository
        Capability cap = agent.getRepository().getCapability(bundleName);
        byte[] buf;
        try {
            buf = cap.getContents();
        } catch (Exception e) {
            logger.severe("Failed to get " + bundleName + " from repository " + e.toString());
            return STATUS.FAILED_NOT_LOADED;
        }
        
        //--execute Capability
        try {
            executorService.installAndStartBundle(bundleName, buf);
        } catch (Exception e) {
            logger.severe("Failed to start bundle " + bundleName + e.toString());
            return STATUS.FAILED_ERROR;
        }
        
        //--return correct status
        EXECUTION_TYPE type = cap.details.getExecutionType();
        if (type == EXECUTION_TYPE.PASSIVE) {
            cyclicCommands.add(cmd);
            logger.finest(cmd.getCommandString() + " PASSIVE Cmd EXECUTED");
            return STATUS.EXECUTED;
        } else if (type == EXECUTION_TYPE.CYCLIC) {
            cyclicCommands.add(cmd);
            logger.finest(cmd.getCommandString() + " CYCLIC Cmd STARTED");
            return STATUS.STARTED;
        } else if (type == EXECUTION_TYPE.ONESHOT) {
            logger.finest(cmd.getCommandString() + " ONESHOT Cmd EXECUTING");
            return STATUS.EXECUTING;
        } else {
            logger.finest(cmd.getCommandString() + " OTHER Cmd EXECUTED");
            return STATUS.EXECUTED;
            //default, set to completed execution
        }
    }

    /**
     * Get an instance of "requester service" from the ExecutorService and
     * start off a 'find' process. If such a service is not available, the 
     * status is set to FAILED_NO_REQUESTER_SERVICE.
     * 
     * @param cmd The command to be executed
     * @return The execution status
     */
    private STATUS find(Command cmd) {
        String[] thingsToFind = cmd.getParameters();
        if (thingsToFind == null || thingsToFind.length == 0) {
            return STATUS.FAILED_INVALID_CMD;
        }
        try {
            RequesterIF requester = (RequesterIF)
                    executorService.getService(RequesterIF.class.getName());
            if (requester == null) {
                logger.finest("Could not get requester. Failing");
                return STATUS.FAILED_NO_REQUESTER_SERVICE;
            }
            if (thingsToFind[0].equals("spec")) {
                logger.finest("Many params, using findBySpec()");
                requester.findBySpec(thingsToFind);
            } else {
                logger.finest("Single param, using findById()");
                requester.findById(thingsToFind[0]);
            }

        } catch (Exception e) {
            logger.finest(e.toString() + " exception was thrown. Failing");
            e.printStackTrace(); //DEBUG
            return STATUS.FAILED_ERROR;
        }
       return STATUS.EXECUTING; 
    }    
    
    private void doArtificialDelay() {
        //ARTIFICIALLY INTRODUCED DELAY FOR EASE OF DEBUGGING
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            logger.finest("Interrupted sleep <" + e.getMessage() + ">");
        }
    }

    /**
     * Method to be used for debugging
     * 
     * @param repository
     */
    private void printRepositoryContents(Repository repository) {
        java.util.Collection<Capability> capabilities = repository.getCapabilities();
        StringBuilder builder = new StringBuilder();
        builder.append("==============================\r\n");
        builder.append("   Agent is at: ");
        builder.append(agent.isAt());
        builder.append("\r\n");
        for (Capability capability : capabilities) {
            builder.append("  cap_id: ");
            builder.append(capability.details.getId());
            builder.append("\r\n");
        }
        builder.append("==============================");
        System.out.println(builder.toString());
    }

}
