/*
 * VERSAG project
 * ----------------------------------------------------------------------------
 * This class is taken from the JADE example source code, originally written by:
 * @author Giovanni Caire - CSELT S.p.A
 * @version $Date: 1/12/2008 19:23:59 $ $Revision: 1.2 $
 */
package mma2.jade;

import jade.core.*;
import jade.core.behaviours.*;
import jade.domain.mobility.*;
import jade.lang.acl.*;
import jade.util.Logger;

import java.io.IOException;
import java.io.Serializable;

import mma2.AgentIF;

/**
 * This behaviour of the Agent serves all the received messages. 
 * The following expressions are accepted as content of ACL REQUEST messages:
 * - (itinerary) load the agent with the itinerary sent in the message
 * - (move <destination>)  to move the Agent to another container. Example (move Front-End) or
 * - (exit) exit the agent
 * - (clone) clone the agent
 * - (status-request) ask for agent internal status
 * Accepting an itinerary should be the main task of this behaviour. Other tasks
 * should be requested through an itinerary in future.
 * 
 */
public class MessageListeningBehaviour extends CyclicBehaviour {

    private static final String CMD_ITINERARY = "itinerary";
    private static final String CMD_PARAMETERS = "parameters";
    private static final String CMD_STATUS_REQUEST = "status-request";
    private static final String CMD_CLONE = "clone";
    private static final String CMD_CURRENT_LOC = "where-are-you";
    private static final String CMD_EXIT = "exit";
    private static final String CMD_MOVE = "move";
    
    private static final String RESPONSE_OK = "\"OK ";
    private static final String RESPONSE_FAIL = "\"FAIL ";
    private static final String RESPONSE_END_QUOTE = "\"";
    
    private transient Logger logger = null;

    public MessageListeningBehaviour(Agent a) {
        super(a);
    }

    public void action() {
        logger = Logger.getMyLogger(getClass().getName());
        MessageTemplate mt = MessageTemplate.MatchPerformative(ACLMessage.REQUEST);

        ACLMessage msg = myAgent.receive(mt);   //non-blocking message reception
        if (msg == null) {
            block();    //no messages so far, block till a message is received
            return;
        } else {
            String replySentence = new String("");
            Serializable responseContent = null;

            // Get action to perform
            String[] content = msg.getContent().split("\n", 2);
            String command = content[0];
            logger.finer("Message received is REQUEST:" + content[0] /*+ ":" + content[1]*/);

            if (command.equals(CMD_ITINERARY)) {
                logger.info(myAgent.getLocalName() + " received a new itinerary");
                replySentence = handleRequestSetItinerary(content[1]);
                ((AgentIF) myAgent).wakeUpKernel();

            } else if (command.equals(CMD_PARAMETERS)) {
                logger.info(myAgent.getLocalName() + " received parameters");
                replySentence = handleRequestSetParameters(content[1]);

            } else if (command.equals(CMD_EXIT)) {
                logger.finer("Exit request received");
                replySentence = new String("\"OK exiting" + RESPONSE_END_QUOTE); //set reply sentence
                myAgent.doDelete();

            } else if (command.equals(CMD_MOVE)) {
                String destination = content[1];
                replySentence = new String(RESPONSE_OK + "moving to " + destination + RESPONSE_END_QUOTE);
                move(destination);

            } else if (command.equals(CMD_CLONE)) {
                String destination = content[1];
                replySentence = new String(RESPONSE_OK + "cloning to " + destination + RESPONSE_END_QUOTE);
                clone(destination);

            } else if (command.equals(CMD_CURRENT_LOC)) {
                Location current = myAgent.here();
                replySentence = current.getName(); //set reply sentence

            } else if (command.equals(CMD_STATUS_REQUEST)) {
                replySentence = ((AgentIF) myAgent).getAgentStatus();

            } else {
                logger.finer("Unknown command " + command);
                replySentence = new String(RESPONSE_FAIL + "unknown command" + RESPONSE_END_QUOTE);
            }

            // Prepare and send response
            ACLMessage replyMsg = msg.createReply();
            replyMsg.setPerformative(ACLMessage.INFORM);
            try {
                if (responseContent != null) {
                    //not using setContentObject() just to be safe (2008-03-07)
                    java.io.ByteArrayOutputStream c = new java.io.ByteArrayOutputStream();
                    java.io.ObjectOutputStream oos = new java.io.ObjectOutputStream(c);
                    oos.writeObject(responseContent);
                    oos.flush();
                    replyMsg.setByteSequenceContent(c.toByteArray());
                } else {
                    replyMsg.setContent(replySentence);
                }
            } catch (IOException e) {
                logger.warning("Failed to set content object in response " + e.toString());
            }
            myAgent.send(replyMsg);
        }
        return;
    }

    private String handleRequestSetItinerary(String itinerary) {
        ((AgentIF) myAgent).getItineraryService().setItinerary(itinerary);
        return RESPONSE_OK + "received" + RESPONSE_END_QUOTE;
    }

    private String handleRequestSetParameters(String parameters) {
        ((AgentIF) myAgent).getRepository().setParameters(parameters);
        return RESPONSE_OK + "received" + RESPONSE_END_QUOTE;
    }
    private void move(String destination) {
        Location dest = new jade.core.ContainerID(destination, null);
        logger.finer("Requested to move to " + destination);
        myAgent.doMove(dest);
    }

    private void clone(String destination) {
        Location dest = new jade.core.ContainerID(destination, null);
        logger.finer("Requested to clone myself to " + destination);

        myAgent.doClone(dest, myAgent.getName() + "." + System.currentTimeMillis());
    }
}
