/*
 * VERSAG project
 */
package mma2.service.execution;

import ch.ethz.iks.concierge.framework.FrameworkV;
import jade.util.Logger;
import java.util.HashMap;
import java.util.Properties;
import java.util.Vector;

import mma2.itinerary.Command.STATUS;
import mma2.bundles.common.ServiceIF;
import mma2.util.Utility;

import org.osgi.framework.Bundle;
import org.osgi.framework.BundleException;


/**
 * The Capability Execution Service. 
 * Since Capabilities are represented as OSGi bundles, an internal 
 * OSGi framework instance is used to execute bundles.
 * 
 * NOTES:
 * Non-serializable class, should be recreated after migration
 * 
 * @author kutila
 */
public class ExecutorService implements ServiceIF {

    private String corePackages;
    
    private Logger logger = null;
    
    /** the OSGi container */
    private FrameworkV concierge = null;

    private Vector<Long> bundleIdVec = new Vector();
    
    /** Contains IDs of installed bundles. May contain invalid records 
     *  if bundles are uninstalled without going through this service 
     */
    private HashMap<String, Long> installedCapabilities = new HashMap();
    
    private boolean osgiStarted = false;

    /**
     * Initialize the Executor Service. Creates required objects and starts
     * the OSGi framework.
     */
    public void init(String corePackages, String name) {
        if (logger == null) {
            logger = Logger.getMyLogger(this.getClass().getName());
        }
        this.corePackages = corePackages;
        
        if (!osgiStarted) {
            startOSGi(name);
            osgiStarted = true;
        }
        if (bundleIdVec == null) {
            bundleIdVec = new Vector();
        }
        if (installedCapabilities == null) {
            installedCapabilities = new HashMap();
        }
    }

    /**
     * Stop the Executor Service by shutting down the OSGi framework
     * (e.g. before moving)
     */
    @Override
    public void requestStop() {
        if (!osgiStarted) {
            return;
        }
        logger.fine("Stopping OSGi framework");
        if (concierge != null) {
            concierge.stopContainer(true); //should be a blocking call??
        }
        osgiStarted = false;
    }

    /**
     * Register the given object as an OSGi service
     */
    public void registerService(Object serviceRef, String serviceClassName) {
        logger.fine("Registering " + serviceClassName + " as a service");

        concierge.registerService(serviceRef, new String[]{serviceClassName});
    }

    /**
     * Register the given object as an OSGi service under multiple class names
     */
    public void registerService(Object serviceRef, String[] serviceClassNames) {
        logger.fine("Registering " + serviceClassNames + " as a service");

        concierge.registerService(serviceRef, serviceClassNames);
    }
    
    /**
     * Retrieve a registered OSGi service for use by the Agent
     * 
     * @param serviceName Class name of the required service
     * @return a reference to the service or null if service unavailable
     */
    public Object getService(String serviceName) {
        logger.fine("Retrieving service " + serviceName);

        return concierge.getService(serviceName);
    }
    
    /**
     * 
     * @param bundleName
     * @param buf The bytes of the bundle JAR
     * @throws java.lang.Exception If an error occurs
     */
    public void installAndStartBundle(String bundleName, byte[] buf) throws Exception {
        try {
            //logger.finest("About to install/start bundle " + bundleName); //DEBUG
            long t1 = System.currentTimeMillis(); //Instrumenting
            long id = concierge.installBundle(bundleName, buf, true);
            installedCapabilities.put(bundleName, id);
            Utility.log(bundleName + " T_start (ms) = " +
                    (System.currentTimeMillis() - t1)); //Instrumenting
            logger.fine("installed/started bundle " + bundleName + " with id " + id);
        } catch (BundleException ex) {
            logger.throwing(this.getClass().getName(), "installAndStartBundle", ex);
            throw new Exception("Failed to install/start bundle " + bundleName, ex);
        }
    }

    /**
     * Stop the specified bundle and uninstall it from OSGi. However
     * it is kept in the Repository and can be "started" later.
     * @param bundleName Name of Bundle to remove
     */
    public void stopAndUninstallBundle(String bundleName) {
        concierge.stopBundle(bundleName, true);
    }
    
    /**
     * Retrieve the status of the specified bundle. If the bundle is uninstalled
     * or not ACTIVE it is assumed that it has completed execution.
     * 
     * @param bundleName
     * @return the current STATUS of the bundle
     */
    public STATUS getBundleStatus(String bundleName) {
        Bundle bundle = concierge.getBundle(bundleName);
        if (bundle != null && bundle.getState() == Bundle.ACTIVE) {
            return STATUS.EXECUTING;
        }
        return STATUS.EXECUTED;
    }


    //----- private methods -----
    
    /**
     * Starts the underlying OSGi framework
     */
    private void startOSGi(String name) {

        logger.fine("Starting OSGi framework");
        Properties props = new Properties();
//        props.setProperty("osgi.auto.install",
//                "bundles/shell-1.0_RC2.jar " +
//                "bundles/concierge_gui-1.0_RC2.jar " +
//                "bundle/event-admin-1.0_RC2.jar " +
//                "bundle/service-tracker-1.0_RC2.jar");
//        props.setProperty("osgi.auto.start",
//                "bundles/shell-1.0_RC2.jar " +
//                "bundles/concierge_gui-1.0_RC2.jar " +
//                "bundle/event-admin-1.0_RC2.jar  " +
//                "bundle/service-tracker-1.0_RC2.jar");
//        System.setProperty("osgi.init", "true"); //not needed, set inside Framework.java
        props.setProperty("org.osgi.framework.system.packages", corePackages);
        props.setProperty("ch.ethz.iks.concierge.embedded", "true");
        
        // create and start an instance of the framework
        try {
            concierge = new FrameworkV();
            concierge.startContainer(props, name);
        } catch (Exception ex) {
            logger.severe("Could not start OSGi framework " + ex.toString());
        }
    }

}
