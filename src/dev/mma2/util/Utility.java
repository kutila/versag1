/*
 * VERSAG project
 */
package mma2.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import jade.util.Logger;
import mma2.capability.CapabilityDetails;
import mma2.capability.Capability;
import mma2.capability.CapabilityDetails.AGENT_PLATFORMS;

/**
 * This class contains utility methods
 *
 * @author kutila
 * created on 31/10/2008
 */
public class Utility {

    public static final String BUNDLE_DIRECTORY = "bundledirectory";
    public static final String BUNDLE_COUNT = "bundlecount";
    public static final String BUNDLE_PREFIX = "bundle.";
    public static final String JAR_SUFFIX = ".jar";
    public static final String DESCRIPTION = ".description";
    public static final String NAME = ".name";
    public static final String VERSION = ".version";
    public static final String TYPE = ".type";
    public static final String FUNCTIONS = ".functions";
    public static final String PLATFORMS = ".platforms";
    public static final String ESTIMATE_RUN_TIME = ".runtime";
    public static final String ESTIMATE_START_TIME = ".starttime";
    public static final String ESTIMATE_RUN_LOAD = ".nwload";
    public static final String ESTIMATE_ACCURACY_RATING = ".accuracy";
    public static final String ESTIMATE_MAX_HEAP = ".heap";
    public static final String ESTIMATE_CPU_RATING = ".cpu";

    public static final String PARAMETERS = "parameters";

    public static final String TYPE_PASSIVE = "passive";
    public static final String TYPE_ACTIVE_ONESHOT = "active_oneshot";
    public static final String TYPE_ACTIVE_CYCLIC = "active_cyclic";
    
    
    public static final String SERVICE_AGENT = "service.agent";
    public static final String SERVICE_REPOSITORY = "service.repository";
    public static final String SERVICE_COUNT = "service.count";
    public static final String SERVICE_PREFIX = "service.";
    public static final String SERVICE_CLASS_SUFFIX = ".classname";
    
    public static final String CORE_PACKAGES = "corepackages";

    public static final String GH2_2 = "GH2_2";
    public static final String JADE2 = "JADE2";
    public static final String JADE3_5 = "JADE3_5";
    public static final String JADE3_7 = "JADE3_7";
    public static final String JADE4 = "JADE4";

    /**
     * Create a Capability object using the given data.
     * 
     * @param props
     * @param i
     * @return
     * @throws java.io.IOException If any failure occurs
     */
    public static Capability loadCapabilityFromFile(
            Properties props, int i) throws IOException {

        String path = props.getProperty(Utility.BUNDLE_DIRECTORY, ".");
        String jarName = props.getProperty(Utility.BUNDLE_PREFIX + i + Utility.JAR_SUFFIX);
        String description = props.getProperty(Utility.BUNDLE_PREFIX + i + Utility.DESCRIPTION,
                "no description");
        String bundleName = props.getProperty(Utility.BUNDLE_PREFIX + i + Utility.NAME);
        String version = props.getProperty(Utility.BUNDLE_PREFIX + i + Utility.VERSION);
        String type = props.getProperty(Utility.BUNDLE_PREFIX + i + Utility.TYPE);
        String functions = props.getProperty(Utility.BUNDLE_PREFIX + i + Utility.FUNCTIONS, "generic");
        String platforms = props.getProperty(Utility.BUNDLE_PREFIX + i + Utility.PLATFORMS, "JADE3_7");
        String estimateStartTime = props.getProperty(Utility.BUNDLE_PREFIX + i + Utility.ESTIMATE_START_TIME, "10");
        String estimateRunTime = props.getProperty(Utility.BUNDLE_PREFIX + i + Utility.ESTIMATE_RUN_TIME);
        String estimateRunLoad = props.getProperty(Utility.BUNDLE_PREFIX + i + Utility.ESTIMATE_RUN_LOAD);
        String estimateAccuracy = props.getProperty(Utility.BUNDLE_PREFIX + i + Utility.ESTIMATE_ACCURACY_RATING, "5");
        String estimateHeap = props.getProperty(Utility.BUNDLE_PREFIX + i + Utility.ESTIMATE_MAX_HEAP);
        String estimateCpu = props.getProperty(Utility.BUNDLE_PREFIX + i + Utility.ESTIMATE_CPU_RATING, "5");

        byte[] buf = readFileIntoByteArray(path + File.separatorChar + jarName + ".jar");
        if (buf == null || buf.length == 0) {
            throw new IOException("Jar file was empty");
        }
        CapabilityDetails details = new CapabilityDetails();
        details.setId(bundleName);
        details.setDescription(description);
        details.setVersion(version);
        details.setFunctions(functions.split(":"));
        details.setSize(buf.length);
        
        details.setEstimatedRunningTime(new Long(estimateRunTime));
        details.setEstimatedStartingTime(new Long(estimateStartTime));
        details.setRunningNwLoad(new Integer(estimateRunLoad));
        details.setAccuracyRating(new Integer(estimateAccuracy));
        details.setCpuRating(new Integer(estimateCpu));
        details.setHeapSizeRating(new Integer(estimateHeap));

        //set agent platforms
        String[] p = platforms.split(":");
        AGENT_PLATFORMS[] agentPlatforms = new AGENT_PLATFORMS[p.length];
        for (int j = 0; j < p.length; j++) {
            if (p[j].equals(GH2_2)) {
                agentPlatforms[j] = AGENT_PLATFORMS.GH2_2;
            } else if (p[j].equals(JADE2)) {
                agentPlatforms[j] = AGENT_PLATFORMS.JADE2;
            } else if (p[j].equals(JADE3_5)) {
                agentPlatforms[j] = AGENT_PLATFORMS.JADE3_5;
            } else if (p[j].equals(JADE3_7)) {
                agentPlatforms[j] = AGENT_PLATFORMS.JADE3_7;
            } else if (p[j].equals(JADE4)) {
                agentPlatforms[j] = AGENT_PLATFORMS.JADE4;
            }
        }
        details.setAgentPlatforms(agentPlatforms);

        if (type.equals(TYPE_PASSIVE)) {
            details.setExecutionType(CapabilityDetails.EXECUTION_TYPE.PASSIVE);
        } else if (type.equals(TYPE_ACTIVE_ONESHOT)) {
            details.setExecutionType(CapabilityDetails.EXECUTION_TYPE.ONESHOT);
        } else {//active cyclic
            details.setExecutionType(CapabilityDetails.EXECUTION_TYPE.CYCLIC);
        }

        Capability cap = new Capability();
        cap.details = details;
        cap.setContents(buf);

        return cap;
    }

    /**
     * Read a file's content into a byte array.
     * 
     * @param fileName Name of file to be read
     * @return a byte array filled with the file contents
     * @throws java.io.IOException If a failure occurs
     */
    public static byte[] readFileIntoByteArray(String fileName) throws IOException {
        byte[] b = null;
        try {
            FileInputStream in = new FileInputStream(fileName);
            int size = in.available();
            b = new byte[size];
            in.read(b);
        } catch (IOException ex) {
            throw new IOException(fileName + " could not be read into a byte array." +
                    ex.toString());
        }
        return b;
    }

    //----- Setting up a globally available "Config Logger" [15/07/2010] -----
    private static Logger configLogger;
    private static volatile boolean logInitialized = false;

    /**
     * Write the log message to the Config Logger at the default Level.CONFIG
     * 
     * @param record
     */
    public static void log(String record) {
        log(Level.CONFIG, record);
    }

    /**
     * Write the log message to Config logger.
     * @param level
     * @param record
     */
    public static void log(Level level, String record) {
        if (!logInitialized) {
            try {
                configLogger = Logger.getMyLogger("CONFIGS");
                FileHandler f = new FileHandler("configs.log", true);
                configLogger.addHandler(f);
                configLogger.setUseParentHandlers(false);
                logInitialized = true;
                System.err.println("CONFIGS logger initialized");
            } catch (Exception e) {
                System.err.println("<<<Failed to setup CONFIGS logger handler>>>");
            }
        }
        configLogger.config(record);
    }
}
