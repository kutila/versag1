/*
 * VERSAG project
 * DSSE Centre, Monash University, Australia
 */
package mma2.util;

/**
 *
 *
 * @author kutila
 * created on 17/05/2009
 */
public class Version {

    //Todo: the date part should be automatically updated when compiling
    public static final String VERSION = "Versag_1.5_20110215";
    public static final String BUILD_TIMESTAMP = "2011 February 15, 2.10pm";

    public static void main(String[] args) {
        System.out.println("VERSAG framework");
        System.out.println("Version:         " + VERSION);
        System.out.println("Build timestamp: " + BUILD_TIMESTAMP);
    }

}
