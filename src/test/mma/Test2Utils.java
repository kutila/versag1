/*
 * VERSAG project
 * DSSE Centre, Monash University, Australia
 */
package mma;

import junit.framework.TestCase;

/**
 *
 * @author kutila
 * created 22/01/2009
 */
public class Test2Utils extends TestCase {

    public void testOne() {
        String[] items = {"new file.txt", "someOther.pdf"};
        for (String item : items) {
            String fileType = item.substring(item.lastIndexOf('.') + 1);
            System.out.println(fileType);    
        }
        
    }
    
    public void testTwo() {
        String text = "The quick brown fox jumped over the fence.\n" +
                "Unfortunately it fell into a well.";
        String[] words = text.split("\\s");
        assertEquals("Wrong number of words counted", 14, words.length);
    }
    
}
