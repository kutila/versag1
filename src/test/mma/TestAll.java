/*
 * TestAll.java
 *
 * Created on 5 December 2007, 16:43
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package mma;

import junit.framework.*;
import mma2.capability.CapabilitySpecTest;
import mma2.capability.CapabilityDetailsTest;

/**
 *
 * @author kutila
 */
public class TestAll extends TestCase {
    
    /** Creates a new instance of TestAll */
    public TestAll() {
    }

    public static void main(String[] args) {
        TestSuite suite = new TestSuite("All MMA junit tests");
        
        //bundle related tests
        suite.addTestSuite(CapabilityDetailsTest.class);
        suite.addTestSuite(CapabilitySpecTest.class);
        
        junit.textui.TestRunner.run(suite);
    }
    
}
