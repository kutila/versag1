/*
 * VERSAG project
 * DSSE Centre, Monash University, Australia
 */
package mma2.capability;

import junit.framework.TestCase;
import mma2.capability.CapabilityDetails.AGENT_PLATFORMS;

/**
 *
 * @author kutila2
 */
public class CapabilityDetailsTest extends TestCase {
    
    public CapabilityDetailsTest(String testName) {
        super(testName);
    }

    /**
     * Test of match() method
     */
    public void testMatch() {
        Object[] supportedFunctions = {"function1", "function2"};
        AGENT_PLATFORMS[] supportedPlatforms = {AGENT_PLATFORMS.JADE3_5, AGENT_PLATFORMS.JADE3_7};

        Object[] neededFunctions = {"function1"};
        AGENT_PLATFORMS[] neededPlatforms = {AGENT_PLATFORMS.JADE3_7};

        boolean result = doComparison(supportedFunctions, supportedPlatforms,
                neededPlatforms, neededFunctions);
        assertTrue("Correct not identify supported subset", result);


        Object[] supportedFunctions2 = {"function2", "function1"};
        result = doComparison(supportedFunctions, supportedPlatforms,
                supportedPlatforms, supportedFunctions2);
        assertTrue("Correct not identify exact match", result);


        Object[] extraF = {"function1", "function3"};
        result = doComparison(supportedFunctions, supportedPlatforms,
                neededPlatforms, extraF);
        assertTrue("Could not identify unsupported function", !result);


        AGENT_PLATFORMS[] extraPlatform = {AGENT_PLATFORMS.GH2_2, AGENT_PLATFORMS.JADE3_7};
        result = doComparison(supportedFunctions, supportedPlatforms,
                extraPlatform, neededFunctions);
        assertTrue("Could not identify unsupported platform", !result);

    }

    //helper method for testMatch()
    private boolean doComparison(
            Object[] supportedFunctions,
            AGENT_PLATFORMS[] supportedPlatforms,
            AGENT_PLATFORMS[] neededPlatforms,
            Object[] neededFunctions) {

        CapabilityDetails details = new CapabilityDetails();
        details.setFunctions(supportedFunctions);
        details.setAgentPlatforms(supportedPlatforms);
        CapabilitySpec spec = new CapabilitySpec();
        spec.setAgentPlatforms(neededPlatforms);
        spec.setFunctions(neededFunctions);
        boolean result = details.match(spec);
        return result;
    }



}
