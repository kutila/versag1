/*
 * VERSAG project
 * DSSE Centre, Monash University, Australia
 */
package mma2.capability;

import mma2.capability.CapabilityDetails.AGENT_PLATFORMS;

import junit.framework.TestCase;

/**
 *
 * @author kutila
 */
public class CapabilitySpecTest extends TestCase {

    public CapabilitySpecTest(String testName) {
        super(testName);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    public void testEquals() {
        CapabilitySpec spec1 = new CapabilitySpec();
        CapabilitySpec spec2 = new CapabilitySpec();

        try {
            spec1.equals(spec2);
            fail("Equals should have thrown an Exception");
        } catch (UnsupportedOperationException uoe) {
            assertNotNull(uoe);
        }
    }
}
