/*
 * VERSAG project
 * DSSE Centre, Monash University, Australia
 */
package simple.cyclic1;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

/**
 * Bundle activator for test "cyclic" bundle
 *
 * @author kutila
 * created on 30/06/2009
 */
public class Cyclic1Activator implements BundleActivator  {
    
    private CyclicClass theService;
    
    @Override
    public void start(BundleContext context) throws Exception {
        theService = new CyclicClass(context);
        theService.start();
        log("Service started: " + CyclicClass.class.getName());
    }
    
    @Override
    public void stop(BundleContext context) throws Exception {
        theService.requestStop();
        log("Service stopped: " + CyclicClass.class.getName());
    }

    private void log(String msg) {
        System.out.println("[Cyclic1Activator] " + msg);
    }

}
