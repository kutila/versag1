/*
 * VERSAG project
 * DSSE Centre, Monash University, Australia
 */
package simple.cyclic1;

import mma2.bundles.common.ServiceIF;
import org.osgi.framework.BundleContext;


/**
 * A simple test "cyclic" bundle
 * 
 * @author kutila
 * created 30/06/2009
 */
public class CyclicClass extends Thread implements ServiceIF {

    private BundleContext context;
    private boolean stop = false;

    public CyclicClass(BundleContext context) {
        super("CyclicThread");
        this.context = context;
    }
    
    @Override
    public void run() {
        System.out.println("***CyclicClass  started***");
        while (!isStopRequested()) {
            //do something
            System.out.print(" :");
            try {Thread.sleep(100);} catch (InterruptedException e) {}
        }
        System.out.println("***CyclicClass finished***");
    }
    
    public synchronized boolean isStopRequested() {
        return stop;
    }
    
    public synchronized void requestStop() {
        stop = true;
    }

}
