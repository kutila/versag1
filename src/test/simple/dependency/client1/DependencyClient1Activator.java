/*
 * VERSAG project
 * DSSE Centre, Monash University, Australia
 */
package simple.dependency.client1;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

/**
 * A client bundle to test dependency
 * Calls the "Hello" class which is in a different bundle
 *
 * Execution type: PASSIVE
 *
 * @author kutila
 */
public class DependencyClient1Activator implements BundleActivator  {

    @Override
    public void start(BundleContext context) throws Exception {
        System.out.println("Starting: " + this.getClass().getName());
        simple.dependency.parent.Hello hello = new simple.dependency.parent.Hello();
        System.out.println(hello.sayHello("John Smith"));
    }

    @Override
    public void stop(BundleContext context) throws Exception {
        System.out.println("Stopping: " + this.getClass().getName());
    }


}
