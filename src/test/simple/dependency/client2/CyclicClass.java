/*
 * VERSAG project
 * DSSE Centre, Monash University, Australia
 */
package simple.dependency.client2;

import mma2.bundles.common.ServiceIF;
import org.osgi.framework.BundleContext;
import simple.dependency.parent.MyTimer;


/**
 * Dependency testing "cyclic" class
 * 
 * @author kutila
 * created 30/06/2009
 */
public class CyclicClass extends Thread implements ServiceIF {

    private BundleContext context;
    private boolean stop = false;

    public CyclicClass(BundleContext context) {
        this.context = context;
    }
    
    @Override
    public void run() {
        MyTimer timer = new MyTimer();
        while (!isStopRequested()) {
            //do something
            System.out.print(timer.getTime());
            try {Thread.sleep(100);} catch (InterruptedException e) {}
        }
        System.out.println("***CyclicClass finished***");
    }
    
    public synchronized boolean isStopRequested() {
        return stop;
    }
    
    public synchronized void requestStop() {
        stop = true;
    }

}
