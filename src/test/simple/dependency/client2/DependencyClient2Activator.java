/*
 * VERSAG project
 * DSSE Centre, Monash University, Australia
 */
package simple.dependency.client2;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

/**
 * Dependency testing client 2.
 * A separate thread which accesses a dependent bundle is started
 *
 * Execution type: ACTIVE_CYCLIC
 *
 * @author kutila2
 */
public class DependencyClient2Activator implements BundleActivator  {

    private CyclicClass cyclicClass;

    @Override
    public void start(BundleContext context) throws Exception {
        System.out.println("Starting: " + this.getClass().getName());
        cyclicClass = new CyclicClass(context);
        cyclicClass.start();
    }

    @Override
    public void stop(BundleContext context) throws Exception {
        System.out.println("Stopping: " + this.getClass().getName());
        cyclicClass.requestStop();
    }


}
