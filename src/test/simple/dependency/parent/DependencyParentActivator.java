/*
 * VERSAG project
 * DSSE Centre, Monash University, Australia
 */
package simple.dependency.parent;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

/**
 * Hello bundle with 1 internal class
 * Simply prints 'hello world' when started.
 *
 * Execution type: PASSIVE
 *
 * @author kutila2
 */
public class DependencyParentActivator implements BundleActivator  {

    @Override
    public void start(BundleContext context) throws Exception {
        System.out.println("Starting: " + this.getClass().getName());
    }

    @Override
    public void stop(BundleContext context) throws Exception {
        System.out.println("Stopping: " + this.getClass().getName());
    }


}
