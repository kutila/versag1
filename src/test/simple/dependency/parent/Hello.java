/*
 * VERSAG project
 * DSSE Centre, Monash University, Australia
 */
package simple.dependency.parent;

/**
 * Class to say Hello for "dependency parent" bundle
 * 
 * @author kutila2
 */
public class Hello {

    public String sayHello() {
        return "     <<<Hello from Hello1Class!>>>";
    }

    public String sayHello(String name) {
        return "     <<<Hello " + name + "! (from Hello1Class)>>>";
    }
}
