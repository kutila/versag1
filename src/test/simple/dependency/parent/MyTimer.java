/*
 * VERSAG project
 * DSSE Centre, Monash University, Australia
 */
package simple.dependency.parent;

import java.util.Calendar;

/**
 *
 * @author kutila2
 */
public class MyTimer {

    public String getTime() {
        Calendar cal = Calendar.getInstance();
        String s = cal.get(Calendar.HOUR_OF_DAY) + ":" +
                cal.get(Calendar.MINUTE) + ":" +
                cal.get(Calendar.SECOND);
        return s;
    }

}
