/*
 * VERSAG project
 */
package simple.hello0class;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

/**
 * Hello bundle with only the Activator class.
 * Simply prints 'hello world' when started.
 * 
 * Execution type: PASSIVE
 *
 * @author kutila
 * @created on 26/05/2009
 */
public class Hello0ClassActivator implements BundleActivator  {
    
    @Override
    public void start(BundleContext context) throws Exception {
        System.out.println("Starting: " + this.getClass().getName());
        System.out.println("     <<<Hello World!>>>");
//        try {
//            ServiceReference ref = context.getServiceReference(Hashtable.class.getName());
//            Hashtable table = (Hashtable) context.getService(ref);
//            System.out.println("the table has: " + table.get("key1"));
//        } catch (Exception e) {
//            System.out.println("Failed to get service reference " + e.getMessage());
//        }
    }
    
    @Override
    public void stop(BundleContext context) throws Exception {
        System.out.println("Stopping: " + this.getClass().getName());
    }

}
