/*
 * VERSAG project
 */
package simple.hello1class;

/**
 * Class to say Hello for "Hello1Class" bundle
 * 
 * @author kutila2
 */
public class Hello {

    public String sayHello() {
        return "     <<<Hello from Hello1Class!>>>";
    }

    public String sayHello(String name) {
        return "     <<<Hello " + name + "! (from Hello1Class)>>>";
    }
}
