/*
 * VERSAG project
 */
package simple.hello1class;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

/**
 * Hello bundle with 1 internal class
 * Simply prints 'hello world' when started.
 *
 * Execution type: PASSIVE
 *
 * @author kutila2
 */
public class Hello1ClassActivator implements BundleActivator  {

    @Override
    public void start(BundleContext context) throws Exception {
        System.out.println("Starting: " + this.getClass().getName());
        Hello hello = new Hello();
        System.out.println(hello.sayHello());
    }

    @Override
    public void stop(BundleContext context) throws Exception {
        System.out.println("Stopping: " + this.getClass().getName());
    }


}
