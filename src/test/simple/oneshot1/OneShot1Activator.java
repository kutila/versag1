/*
 * VERSAG project
 * DSSE Centre, Monash University, Australia
 */
package simple.oneshot1;

import mma2.bundles.common.AbstractOneShotClass;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

/**
 * Bundle activator for test "one-shot" bundle
 *
 * @author kutila
 * created on 29/06/2009
 */
public class OneShot1Activator implements BundleActivator  {
    
    private AbstractOneShotClass theService;
    
    @Override
    public void start(BundleContext context) throws Exception {
        theService = new OneShotClass(context);
        //theService = new OneShotClass(context);
        theService.start();
        log("Service started: " + OneShotClass.class.getName());
    }
    
    @Override
    public void stop(BundleContext context) throws Exception {
        theService.requestStop();
        log("Service stopped: " + OneShotClass.class.getName());
    }

    private void log(String msg) {
        System.out.println("[OneShot1Activator] " + msg);
    }

}
