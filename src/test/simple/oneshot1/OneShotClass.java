/*
 * VERSAG project
 * DSSE Centre, Monash University, Australia
 */
package simple.oneshot1;

import java.util.Collection;
import mma2.AgentIF;
import mma2.bundles.common.AbstractOneShotClass;
import mma2.capability.Capability;
import mma2.capability.Repository;
import org.osgi.framework.BundleContext;

/**
 *
 * @author kutila2
 * created on 30/10/2009
 */
public class OneShotClass extends AbstractOneShotClass {

    public OneShotClass(BundleContext context) {
        super(context);
    }

    @Override
    public void work() {
        Repository repository = (Repository) getOSGiService(Repository.class.getName());
        AgentIF agent = (AgentIF) getOSGiService(AgentIF.class.getName());

        Collection<Capability> capabilities = repository.getCapabilities();
        StringBuilder builder = new StringBuilder();
        builder.append("==============================\r\n");
        builder.append("   Agent is at: ");
        builder.append(agent.isAt());
        builder.append("\r\n");
        for (Capability capability : capabilities) {
            builder.append("  cap_id: ");
            builder.append(capability.details.getId());
            builder.append("\r\n");
        }
        builder.append("==============================");
        System.out.println(builder.toString());
    }

}
