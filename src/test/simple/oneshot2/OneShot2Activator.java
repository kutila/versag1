/*
 * VERSAG project
 * DSSE Centre, Monash University, Australia
 */
package simple.oneshot2;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

/**
 * Bundle activator for test "one-shot" bundle
 *
 * @author kutila
 * created on 29/06/2009
 */
public class OneShot2Activator implements BundleActivator  {
    
    private OneShot2Class theService;
    
    @Override
    public void start(BundleContext context) throws Exception {
        theService = new OneShot2Class(context);
        theService.start();
        log("Service started: " + OneShot2Class.class.getName());
    }
    
    @Override
    public void stop(BundleContext context) throws Exception {
        theService.requestStop();
        log("Service stopped: " + OneShot2Class.class.getName());
    }

    private void log(String msg) {
        System.out.println("[OneShot2Activator] " + msg);
    }

}
