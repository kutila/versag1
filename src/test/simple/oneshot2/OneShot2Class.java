/*
 * VERSAG project
 * DSSE Centre, Monash University, Australia
 */
package simple.oneshot2;

import java.util.Collection;

import mma2.AgentIF;
import mma2.bundles.common.AbstractOneShotClass;
import mma2.capability.Capability;
import mma2.capability.Repository;

import org.osgi.framework.BundleContext;


/**
 * A simple test "one-shot" bundle with 2 classes
 * 
 * @author kutila
 * created 29/06/2009
 */
public class OneShot2Class extends AbstractOneShotClass {

    public OneShot2Class(BundleContext context) {
        super(context);
    }
    
    public void work() {
        Repository repository = (Repository) getOSGiService(Repository.class.getName());
        AgentIF agent = (AgentIF) getOSGiService(AgentIF.class.getName());

        Collection<Capability> capabilities = repository.getCapabilities();
        System.out.println("   Agent is at: " + agent.isAt());
        new OneShot2Helper().helpMethod(capabilities);
    }


}
