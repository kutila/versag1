/*
 * VERSAG project
 * DSSE Centre, Monash University, Australia
 */
package simple.oneshot2;

import java.util.Collection;
import mma2.capability.Capability;

/**
 * A helper class for the one-shot 2 bundle
 * 
 * @author kutila
 * created 29/06/2009
 */
public class OneShot2Helper {

    public void helpMethod(Collection<Capability> capabilities) {
        for (Capability capability : capabilities) {
            System.out.println("  cap_id: " + capability.details.getId());
        }

    }

}
