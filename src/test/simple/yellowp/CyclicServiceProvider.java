/*
 * VERSAG project
 * DSSE Centre, Monash University, Australia
 */
package simple.yellowp;

import mma2.AgentIF;
import mma2.bundles.common.ServiceIF;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;

/**
 *
 * @author kutila2
 * created on 13/08/2010
 */
public class CyclicServiceProvider  extends Thread implements ServiceIF {

    private BundleContext context;
    private boolean stop = false;
    private String SERVICE_NAME = "FirstService";

    public CyclicServiceProvider(BundleContext context) {
        super("CyclicServiceProvider");
        this.context = context;
    }

    @Override
    public void run() {
        System.out.println("***CyclicProvider  started***");

        AgentIF agent = (AgentIF) getOSGiService(AgentIF.class.getName());
        agent.registerInYellowPages(SERVICE_NAME, "Bobs first");
        System.out.println("---Registered " + SERVICE_NAME);


        while (!isStopRequested()) {
            //do something
            System.out.print(" :");
            try {Thread.sleep(100);} catch (InterruptedException e) {}
        }
        System.out.println("***CyclicProvider finished***");
    }

    public synchronized boolean isStopRequested() {
        return stop;
    }

    public synchronized void requestStop() {
        AgentIF agent = (AgentIF) getOSGiService(AgentIF.class.getName());
        agent.deregisterFromYellowPages();
        System.out.println("---Deregistered " + agent.getAgentName());

        stop = true;
    }

    protected Object getOSGiService(String className) {
        ServiceReference ref = context.getServiceReference(className);
        return context.getService(ref);
    }


}

