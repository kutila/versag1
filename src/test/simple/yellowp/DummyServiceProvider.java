/*
 * VERSAG project
 * DSSE Centre, Monash University, Australia
 */
package simple.yellowp;

import mma2.AgentIF;
import mma2.bundles.common.AbstractOneShotClass;
import org.osgi.framework.BundleContext;

/**
 *
 * @author kutila2
 * created on 13/08/2010
 */
public class DummyServiceProvider extends AbstractOneShotClass {

    public DummyServiceProvider(BundleContext context) {
        super(context);
    }

    @Override
    public void work() {
        AgentIF agent = (AgentIF) getOSGiService(AgentIF.class.getName());

        agent.registerInYellowPages("FirstService", "Bobs first");
        System.out.println("---Registered service");

        searchForService(agent, "FirstService");

        agent.deregisterFromYellowPages();
        System.out.println("---Deregistered");

        agent.deregisterFromYellowPages();
        System.out.println("---Deregistered");

        agent.registerInYellowPages("CafeService", "Cafe du Bob");
        System.out.println("---Registered service");

        searchForService(agent, null);
        searchForService(agent, "CafeService");

    }

    private void searchForService(AgentIF agent, String name) {
        String[] ans = agent.searchInYellowPages(name);
        StringBuilder b = new StringBuilder();
        for (String string : ans) {
            b.append(string);
            b.append(',');
        }
        System.out.println("---AVAILABLE SERVICES: <" + b.toString() + ">");

    }

}