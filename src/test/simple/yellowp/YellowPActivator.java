/*
 * VERSAG project
 * DSSE Centre, Monash University, Australia
 */
package simple.yellowp;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;


/**
 *
 * @author kutila2
 * created on 13/08/2010
 */
public class YellowPActivator implements BundleActivator  {

    private CyclicServiceProvider theService;

    @Override
    public void start(BundleContext context) throws Exception {
        //theService = new DummyServiceProvider(context);
        theService = new CyclicServiceProvider(context);
        theService.start();
        log("Service started: " + CyclicServiceProvider.class.getName());
    }

    @Override
    public void stop(BundleContext context) throws Exception {
        theService.requestStop();
        log("Service stopped: " + CyclicServiceProvider.class.getName());
    }

    private void log(String msg) {
        System.out.println("[CyclicServiceProvider] " + msg);
    }

}